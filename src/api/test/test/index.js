import request from '@/utils/request'

export function page(query) {
  return request({
    //url: ' /api/shop2/test/test/page',
    url: ' /api/student/test/test/page',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/api/student/test/test',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/api/student/test/test/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/api/student/test/test/' + id,
    method: 'delete'
  })
}

export function putObj(id, obj) {
  return request({
    url: '/api/student/test/test/' + id,
    method: 'put',
    data: obj
  })
}


